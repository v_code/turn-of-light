# Turn of Light

Turn of Light is a java android application for showing the time of turning off and on the light. 

## How it works
Calculate today's and tomorrow's date. Based on the current time, the next shutdown time is highlighted. By clicking on the button, the day switches from today to tomorrow and vice versa. The days and times are stored in the collection.

![screen_turn_of_light](img/screen_turn_of_light.jpg)
