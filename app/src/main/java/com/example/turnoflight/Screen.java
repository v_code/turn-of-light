package com.example.turnoflight;

import static com.example.turnoflight.day.Day.getNumberDay;
import static com.example.turnoflight.day.DayOfWeek.addDays;
import static com.example.turnoflight.MainActivity.days;
import static com.example.turnoflight.Time.nextShutdownTime;

import android.widget.Button;
import android.widget.TextView;

import com.example.turnoflight.day.Today;
import com.example.turnoflight.day.Tomorrow;

public class Screen {
    private final TextView label;
    private final TextView date;
    private final Button button;

    public Screen(TextView label, TextView date, Button button) {
        this.label = label;
        this.date = date;
        this.button = button;
    }

    public TextView getLabel() {
        return label;
    }

    public TextView getDate() {
        return date;
    }

    public Button getButton() {
        return button;
    }

    public static void setText(TextView label, TextView date, Button button ){
        Screen screen = new Screen( label, date, button );
        Today today = new Today();
        Tomorrow tomorrow = new Tomorrow();
        if ( MainActivity.isToday ) {
            today.setElementText( screen );
        } else {
            tomorrow.setElementText( screen );
        }
    }

    public static void prepareElements() {
        clearView();
        addDays( getNumberDay() );
        nextShutdownTime();
    }

    private static void clearView(){
        if ( !days.isEmpty() ) { days.clear(); }
    }

}
