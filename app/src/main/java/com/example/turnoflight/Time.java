package com.example.turnoflight;

import static com.example.turnoflight.MainActivity.days;
import static com.example.turnoflight.MainActivity.idx;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {
    public static int getCurrentTime(){
        @SuppressLint("SimpleDateFormat")
        DateFormat dateFormat = new SimpleDateFormat("HH");
        return Integer.parseInt( dateFormat.format(new Date()) );
    }

    public static void nextShutdownTime(){
        int currentTime = getCurrentTime();
        for( int i = 0; i < days.size(); i++ ) {
            String lightOff = days.get(i).getLightOff();
            String[] arr = lightOff.split(" ");
            int timeLightOff = Integer.parseInt(arr[2]);
            if (timeLightOff > currentTime) {
                idx = i;
                break;
            } else {
                idx = -1;
            }
        }
    }

}
