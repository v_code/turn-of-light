package com.example.turnoflight.day;

import android.annotation.SuppressLint;
import android.widget.Button;

import com.example.turnoflight.Screen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class AbstractDay {

    public abstract void setElementText( Screen screen );

    public void setButtonIcon(int imgResource, Button btn) {
        btn.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, imgResource, 0);
        btn.setCompoundDrawablePadding(8);
    }

    public static String getDayDate(int count) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, count);
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("EEEE dd.MM.yyyy", Locale.ENGLISH);
        return dateFormat.format(date).toUpperCase();
    }

    @SuppressLint("SimpleDateFormat")
    public static DateFormat getDayName(){
        return new SimpleDateFormat("u");
    }

}
