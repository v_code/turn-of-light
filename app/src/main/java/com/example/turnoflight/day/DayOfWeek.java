package com.example.turnoflight.day;

import static com.example.turnoflight.MainActivity.days;

import java.util.Arrays;
import java.util.List;

public enum DayOfWeek {

    MONDAY_THURSDAY_SUNDAY ( Arrays.asList(
                new Day("03 – 04", "06 – 07"),
                new Day("12 – 13", "15 – 16"),
                new Day("21 – 22", "00 – 01") )),

    TUESDAY_FRIDAY ( Arrays.asList(
                new Day("06 – 07", "09 – 10"),
                new Day("15 – 16", "18 – 19") )),

    WEDNESDAY_SATURDAY ( Arrays.asList(
                new Day("00 – 01", "03 – 04"),
                new Day("09 – 10", "12 – 13"),
                new Day("18 – 19", "21 – 22") ));

    private final List<Day> asList;


    DayOfWeek(List<Day> asList) {
        this.asList = asList;
    }

    public List<Day> getAsList() {
        return asList;
    }

    public static void addDays( int day ){
        switch ( day ) {
            case 1:
            case 4:
            case 7:
                days.addAll(MONDAY_THURSDAY_SUNDAY.getAsList());
                break;
            case 2:
            case 5:
                days.addAll(TUESDAY_FRIDAY.getAsList());
                break;
            default:
                days.addAll(WEDNESDAY_SATURDAY.getAsList());
        }
    }

}
