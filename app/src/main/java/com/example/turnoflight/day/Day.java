package com.example.turnoflight.day;

import static com.example.turnoflight.MainActivity.isToday;
import static com.example.turnoflight.day.AbstractDay.getDayName;

import java.util.Date;

public class Day {
    private final String lightOff;
    private final String lightOn;

    public Day(String lightOff, String lightOn) {
        this.lightOff = lightOff;
        this.lightOn = lightOn;
    }

    public String getLightOff() {
        return lightOff;
    }

    public String getLightOn() {
        return lightOn;
    }

    public static int getNumberDay(){
        int numberDay = Integer.parseInt(getDayName().format(new Date()));
        if ( !isToday ) {
            Tomorrow tomorrow = new Tomorrow(numberDay);
            return tomorrow.getNumberTomorrow();
        }
        return numberDay;
    }

}
