package com.example.turnoflight.day;

import android.widget.Button;

import com.example.turnoflight.R;
import com.example.turnoflight.Screen;

public class Today extends AbstractDay {

    public Today() {}

    @Override
    public void setElementText( Screen screen ) {
        String labelText = "Today";
        String buttonText = "Tomorrow";
        screen.getLabel().setText(labelText.toUpperCase());
        screen.getDate().setText( getDayDate(0) );
        screen.getButton().setText(buttonText.toUpperCase());
        setButtonIcon( R.drawable.eye_30, screen.getButton() );
    }

    @Override
    public void setButtonIcon( int imgResource, Button btn ) {
        super.setButtonIcon(imgResource, btn);
    }

}
