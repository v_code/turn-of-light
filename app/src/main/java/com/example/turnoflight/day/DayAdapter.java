package com.example.turnoflight.day;

import static com.example.turnoflight.MainActivity.idx;
import static com.example.turnoflight.MainActivity.isToday;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.turnoflight.R;

import java.util.List;

public class DayAdapter extends RecyclerView.Adapter<DayAdapter.ViewHolder>{
    private final LayoutInflater inflater;
    private final List<Day> days;

    public DayAdapter(Context context, List<Day> days) {
        this.days = days;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public DayAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DayAdapter.ViewHolder holder, int position) {
        Day day = days.get(position);
        setText( holder, day );
        setColor( holder, position );
    }

    private void setText( DayAdapter.ViewHolder holder, Day day ){
        holder.lightOffView.setText(day.getLightOff());
        holder.lightOnView.setText(day.getLightOn());
    }

    private void setColor( DayAdapter.ViewHolder holder, int position ){
        if ( isToday ) {
            if( position == idx ){
                holder.lightOffView.setTextColor(Color.parseColor("#8D3B72"));
                holder.lightOnView.setTextColor(Color.parseColor("#8D3B72"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return days.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView lightOffView, lightOnView;
        ViewHolder(View view){
            super(view);
            lightOffView = view.findViewById(R.id.lightOff);
            lightOnView = view.findViewById(R.id.lightOn);
        }
    }

}
