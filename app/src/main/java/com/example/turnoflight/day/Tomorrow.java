package com.example.turnoflight.day;

import android.widget.Button;

import com.example.turnoflight.R;
import com.example.turnoflight.Screen;

public class Tomorrow extends AbstractDay {
    private int dayOfWeek;

    public Tomorrow() {}

    public Tomorrow(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getNumberTomorrow(){
        int day = getDayOfWeek();
        if ( day == 7 ){
            day = 1;
        } else {
            day++;
        }
        return day;
    }

    @Override
    public void setElementText( Screen screen ) {
        String labelText = "Tomorrow";
        String buttonText = "Today";
        screen.getLabel().setText(labelText.toUpperCase());
        screen.getDate().setText( getDayDate(1) );
        screen.getButton().setText(buttonText.toUpperCase());
        setButtonIcon( R.drawable.eye_return_30, screen.getButton() );
    }

    @Override
    public void setButtonIcon(int imgResource, Button btn) {
        super.setButtonIcon(imgResource, btn);
    }

}
