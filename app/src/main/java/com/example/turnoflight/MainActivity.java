package com.example.turnoflight;

import static com.example.turnoflight.Screen.setText;
import static com.example.turnoflight.Screen.prepareElements;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.turnoflight.day.AbstractDay;
import com.example.turnoflight.day.Day;
import com.example.turnoflight.day.DayAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static ArrayList<Day> days = new ArrayList<>();
    public static boolean isToday = true;
    public static int idx;
    public TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        date = findViewById(R.id.date);
        date.setText( AbstractDay.getDayDate(0) );
        setView();
    }

    private void setView(){
        prepareElements();
        RecyclerView recyclerView = findViewById(R.id.list);
        DayAdapter adapter = new DayAdapter(this, days);
        recyclerView.setAdapter(adapter);
    }

    @SuppressLint("SetTextI18n")
    public void onClickButton(View view) {
        isToday = !isToday;
        setText( findViewById(R.id.label), date, findViewById(R.id.button_id) );
        setView();
    }

}
